from django.db import IntegrityError
from .models import SalesRecord, AutomobileVO, PotentialCustomer, SalesPerson
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from common.json import ModelEncoder
import requests


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["import_href", "vin", "sold"]


class SalesPersonListEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["id", "sales_person", "employee_number"]


class SalesPersonDetailEncoder(ModelEncoder):
    model = SalesPerson
    properties = ["id", "sales_person", "employee_number"]


class CustomerListEncoder(ModelEncoder):
    model = PotentialCustomer
    properties = ["id", "customer_name", "address", "phone_number"]


class CustomerDetailEncoder(ModelEncoder):
    model = PotentialCustomer
    properties = ["id", "customer_name", "address", "phone_number"]


class SalesRecordListEncoder(ModelEncoder):
    model = SalesRecord
    properties = ["id", "price", "automobile", "sales_rep", "customer"]

    encoders = {
        "automobile": AutomobileVODetailEncoder(),
        "sales_rep": SalesPersonListEncoder(),
        "customer": CustomerListEncoder(),
    }


class SalesRecordDetailEncoder(ModelEncoder):
    model = SalesRecord
    properties = ["id", "price", "automobile", "sales_rep", "customer"]

    encoders = {
        "automobile": AutomobileVODetailEncoder(),
        "sales_rep": SalesPersonListEncoder(),
        "customer": CustomerListEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_salespeople(request):
    if request.method == "GET":
        salespeople = SalesPerson.objects.all().order_by("employee_number")
        return JsonResponse(
            {"salespeople": salespeople},
            encoder=SalesPersonListEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            salesperson = SalesPerson.objects.create(**content)
            return JsonResponse(
                salesperson,
                encoder=SalesPersonDetailEncoder,
                safe=False,
            )
        except IntegrityError:
            return JsonResponse(
                {"message": "Employee number already exists"},
                status=400,
            )


@require_http_methods(["GET"])
def api_show_salesperson(request, pk):
    if request.method == "GET":
        salesperson = SalesPerson.objects.get(id=pk)
        return JsonResponse(
            salesperson,
            encoder=SalesPersonDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_customers(request):
    if request.method == "GET":
        customers = PotentialCustomer.objects.all()
        return JsonResponse({"customers": customers}, encoder=CustomerListEncoder)
    else:
        content = json.loads(request.body)
        customer = PotentialCustomer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerDetailEncoder,
            safe=False,
        )


@require_http_methods(["GET", "POST"])
def api_list_sales_record(request):
    if request.method == "GET":
        sales_record = PotentialCustomer.objects.all()
        return JsonResponse({"autosales": sales_record}, encoder=SalesRecordListEncoder)
    else:
        content = json.loads(request.body)
        try:
            automobile = AutomobileVO.objects.get(vin=content["automobile"])
            content["automobile"] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid automobile vin"},
                status=400,
            )
        try:
            salesperson = content["sales_rep"]
            content["sales_rep"] = PotentialCustomer.objects.get(
                sales_person=salesperson
            )

        except PotentialCustomer.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid salesperson name"},
                status=400,
            )
        try:
            customer = PotentialCustomer.objects.get(customer_name=content["customer"])
            content["customer"] = customer
        except PotentialCustomer.DoesNotExist:
            return JsonResponse({"message": "Invalid customer name"})

        sales_record = PotentialCustomer.objects.create(**content)

        return JsonResponse(
            sales_record,
            encoder=SalesRecordDetailEncoder,
            safe=False,
        )


@require_http_methods(["DELETE"])
def api_delete_sales_record(request, pk):

    count, _ = SalesRecord.objects.filter(id=pk).delete()
    return JsonResponse({"deleted": count > 0})
