from django.urls import path
from .views import (
    api_delete_sales_record,
    api_list_customers,
    api_list_salespeople,
    api_show_salesperson,
    api_list_sales_record,
)

urlpatterns = [
    path("customers/", api_list_customers, name="api_list_customers"),
    path("salespeople/", api_list_salespeople, name="api_list_salespeople"),
    path("salespeople/<int:pk>/", api_show_salesperson, name="api_show_salesperson"),
    path("salesrecords/", api_list_sales_record, name="api_list_sales_records"),
    path(
        "salesrecords/<int:pk>/",
        api_delete_sales_record,
        name="api_delete_sales_record",
    ),
]
