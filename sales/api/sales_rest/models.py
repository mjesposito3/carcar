from django.db import models
from django.urls import reverse

# Create your models here.
class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)

    def __str__(self):
        return self.vin


class SalesPerson(models.Model):
    sales_person = models.CharField(max_length=100)
    employee_number = models.CharField(max_length=50, null=True)

    def __str__(self):
        return self.salesPerson


class PotentialCustomer(models.Model):
    customerName = models.CharField(max_length=150)
    address = models.CharField(max_length=150, null=True, blank=False)
    phone_number = models.CharField(max_length=30, null=True, blank=False)
    sales_person = models.ForeignKey(
        SalesPerson, related_name="+", on_delete=models.PROTECT
    )

    def __str__(self):
        return self.customerName


class SalesRecord(models.Model):
    automobile = models.OneToOneField(
        AutomobileVO, related_name="records", on_delete=models.PROTECT
    )
    sales_person = models.ForeignKey(
        SalesPerson, related_name="records", on_delete=models.PROTECT
    )
    customer = models.ForeignKey(
        PotentialCustomer, related_name="records", on_delete=models.PROTECT
    )
    price = models.CharField(max_length=100, null=True, blank=False)

    def __str__(self):
        return self.sales_person.sales_person + " sold " + str(self.automobile.vin)
