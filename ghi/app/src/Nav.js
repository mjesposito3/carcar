import { NavLink } from 'react-router-dom';
import Dropdown from 'react-bootstrap/Dropdown';


function NavMenu() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item px-2">
              <Dropdown title='inventory'>
                <Dropdown.Toggle variant="dark" id="sales-dropdown">
                  Inventory
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item href="/manufacturers/">Current Manufacturers</Dropdown.Item>
                  <Dropdown.Item href="/manufacturers/new/">Create Manufacturers</Dropdown.Item>
                  <Dropdown.Item href="/models/">Current Models</Dropdown.Item>
                  <Dropdown.Item href="/models/new/">Create Manufacturers</Dropdown.Item>
                  <Dropdown.Item href="/automobiles/">Current Inventory</Dropdown.Item>
                  <Dropdown.Item href="/automobiles/new/">Register New Car</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </li>
            <li className="nav-item px-2">
              <Dropdown title='inventory'>
                <Dropdown.Toggle variant="dark" id="sales-dropdown">
                  Vehicle Service
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item href="/service/">Service Appointments</Dropdown.Item>
                  <Dropdown.Item href="/service/new/">Schedule Service</Dropdown.Item>
                  <Dropdown.Item href="/service/history/">Service Records</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </li>
            <li className="nav-item px-2">
              <Dropdown title='inventory'>
                <Dropdown.Toggle variant="dark" id="sales-dropdown">
                  Vehicle Sales
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item href="/sales/">Sales Records</Dropdown.Item>
                  <Dropdown.Item href="/sales/create/">New Sales</Dropdown.Item>
                  <Dropdown.Item href="/sales/by-rep/">Sales by Employees</Dropdown.Item>
                  <Dropdown.Item href="/customer/new/">Register New Customer</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </li>
            <li className="nav-item px-2">
              <Dropdown title='inventory'>
                <Dropdown.Toggle variant="dark" id="sales-dropdown">
                  Employee Onboarding
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  <Dropdown.Item href="/employee/new/">Register Salesperson</Dropdown.Item>
                  <Dropdown.Item href="/service/technician/">Register Technician</Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </li>
          </ul>
        </div>
      </div >
    </nav >
  )
}

export default NavMenu;
